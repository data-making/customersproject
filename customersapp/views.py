from django.shortcuts import render
from .models import Customer

# Create your views here.

def home(request):

    if request.method == "POST":
        customer_name = request.POST.get("customer_name")
        customer_email = request.POST.get("customer_email")
        customer_city = request.POST.get("customer_city")

        customer_object = Customer(
            customer_name = customer_name,
            customer_email = customer_email,
            customer_city = customer_city
        )

        customer_object.save()

    customers = {"customers": Customer.objects.all()}
    # {"customers": [Customer1, Customer2, Customer3]}
    return render(request, "home.html", context=customers)
