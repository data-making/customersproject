from django.db import models

# Create your models here.

class Customer(models.Model):
    # id
    customer_id = models.AutoField(primary_key=True)
    customer_name = models.CharField(max_length=100)
    customer_email = models.EmailField(max_length=100)
    customer_city = models.CharField(max_length=50)

    class Meta:
        db_table = "customer_tbl"
